##############################################################################
# Copyright (c) 2013-2016, Lawrence Livermore National Security, LLC.
# Produced at the Lawrence Livermore National Laboratory.
#
# This file is part of Spack.
# Created by Todd Gamblin, tgamblin@llnl.gov, All rights reserved.
# LLNL-CODE-647188
#
# For details, see https://github.com/llnl/spack
# Please also see the NOTICE and LICENSE files for our notice and the LGPL.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License (as
# published by the Free Software Foundation) version 2.1, February 1999.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the IMPLIED WARRANTY OF
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the terms and
# conditions of the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
##############################################################################
#
from spack import *
import os
import platform
import spack
from shutil import copyfile

class QrMumps(CMakePackage):
    """a software package for the solution of sparse, linear systems on multicore computers based on the QR factorization of the input matrix."""
    homepage = "http://buttari.perso.enseeiht.fr/qr_mumps/"
    url      = "http://buttari.perso.enseeiht.fr/qr_mumps/"
    svnroot  = "http://buttari.perso.enseeiht.fr/qr_mumps/"

    version('develop', svn='https://www.irit.fr/svn/qr_mumps/branches/qrm_gpu')

    variant('debug', default=False, description='Enable debug symbols')
    variant('cuda', default=False, description='Enable CUDA')
    variant('metis', default=True, description='Enable Metis ordering')
    variant('scotch', default=False, description='Enable Scotch ordering')
    variant('starpu', default=True, description='Enable StarPU runtime system support')
    variant('fxt', default=False, description='Enable FxT tracing support to be used through StarPU')
    variant('d', default=True, description='Build QrMumps in double precision')
    variant('s', default=False, description='Build QrMumps in simple precision')
    variant('z', default=False, description='Build QrMumps in complex double precision')
    variant('c', default=False, description='Build QrMumps in complex simple precision')

    depends_on("blas",type=('run'))
    depends_on("lapack",type=('run'))
    depends_on("metis", when='+metis')
    depends_on("scotch", when='+scotch')
    depends_on("starpu", when='+starpu')
    depends_on("cuda", when='+cuda')
    depends_on("starpu+cuda", when='+cuda')
    depends_on("starpu+fxt", when='+starpu+fxt')


    def cmake_args(self):
        spec = self.spec
        args = std_cmake_args
        # stagedir=self.stage.path+'/qrm_gpu' # with
        # working_dir('spack-build',create=True):
        ariths="d"
        args.extend([
            "-Wno-dev",
            "-DARITH=%s"               % ariths,
            "-DCMAKE_BUILD_TYPE=%s"    % ('Debug' if '+debug'  in spec else 'Release'),
            "-DQRM_WITH_STARPU=%s"     % ('ON'    if '+starpu' in spec else 'OFF'    ),
            "-DQRM_ORDERING_SCOTCH=%s" % ('ON'    if '+scotch' in spec else 'OFF'    ),
            "-DQRM_ORDERING_METIS=%s"  % ('ON'    if '+metis'  in spec else 'OFF'    ),
            "-DQRM_WITH_CUDA=%s"       % ('ON'    if '+cuda'   in spec else 'OFF'    )
        ])

        # # StarPU if spec.satisfies('+starpu'):
        # args.extend(["-DSTARPU_DIR=%s" % spec['starpu'].prefix])

        # Blas
        blas_libs = spec['blas'].libs.ld_flags
        args.extend(["-DBLAS_LIBRARIES=%s" % blas_libs])

        # Lapack
        lapack_libs = spec['lapack'].libs.ld_flags
        args.extend(["-DLAPACK_LIBRARIES=%s" % lapack_libs])

        return args


